import 'package:coldshowerchallenge/components/faderoute.dart';
import 'package:coldshowerchallenge/components/sliderightroute.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:coldshowerchallenge/screens/about/aboutscreen.dart';
import 'package:coldshowerchallenge/screens/home/homescreen.dart';
import 'package:coldshowerchallenge/screens/leaderboard/leadsscreen.dart';
import 'package:coldshowerchallenge/screens/login/loginscreen.dart';
import 'package:coldshowerchallenge/screens/profile/profilescreen.dart';
import 'package:coldshowerchallenge/screens/profile/userscreen.dart';
import 'package:coldshowerchallenge/screens/root/rootscreen.dart';
import 'package:coldshowerchallenge/screens/settings/settingsscreen.dart';
import 'package:coldshowerchallenge/screens/shower/detailscreen.dart';
import 'package:coldshowerchallenge/screens/shower/showerscreen.dart';
import 'package:coldshowerchallenge/screens/stats/statsscreen.dart';
import 'package:coldshowerchallenge/services/authentication.dart';
import 'package:coldshowerchallenge/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;
import 'package:flutter/services.dart';
import 'package:flutter_statusbar_text_color/flutter_statusbar_text_color.dart';

void main() {
  runApp(CSCApp());
}

class CSCApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    globals.auth = new Auth(globals.repository);
    return MaterialApp(
      title: 'Cold Shower Challenge',
      theme: appTheme(context),
      home: RootScreen(auth: globals.auth),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case Routes.login:
            return FadeRoute(page: LoginScreen(auth: globals.auth));
          case Routes.shower:
            return SlideRightRoute(page: ShowerScreen(), settings: settings);
          case Routes.home:
            return FadeRoute(page: HomePage());
          case Routes.profile:
            return FadeRoute(page: ProfileScreen());
          case Routes.stats:
            return FadeRoute(page: StatsScreen());
          case Routes.leaderboard:
            return FadeRoute(page: LeadsScreen());
          case Routes.settings:
            return FadeRoute(page: SettingsScreen());
          case Routes.about:
            return FadeRoute(page: AboutScreen());
          case Routes.detail:
            return SlideRightRoute(page: ShowerDetailScreen(), settings: settings);
          case Routes.user:
            return SlideRightRoute(page: UserScreen(), settings: settings);
        }
        return FadeRoute(page: HomePage());
      },
    );
  }
}
