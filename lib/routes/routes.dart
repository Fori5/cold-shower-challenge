import 'package:coldshowerchallenge/screens/about/aboutscreen.dart';
import 'package:coldshowerchallenge/screens/home/homescreen.dart';
import 'package:coldshowerchallenge/screens/leaderboard/leadsscreen.dart';
import 'package:coldshowerchallenge/screens/login/loginscreen.dart';
import 'package:coldshowerchallenge/screens/profile/profilescreen.dart';
import 'package:coldshowerchallenge/screens/profile/userscreen.dart';
import 'package:coldshowerchallenge/screens/settings/settingsscreen.dart';
import 'package:coldshowerchallenge/screens/shower/detailscreen.dart';
import 'package:coldshowerchallenge/screens/shower/showerscreen.dart';
import 'package:coldshowerchallenge/screens/stats/statsscreen.dart';

class Routes {
  static const String login = LoginScreen.routeName;
  static const String shower = ShowerScreen.routeName;
  static const String home = HomePage.routeName;
  static const String profile = ProfileScreen.routeName;
  static const String stats = StatsScreen.routeName;
  static const String leaderboard = LeadsScreen.routeName;
  static const String settings = SettingsScreen.routeName;
  static const String about = AboutScreen.routeName;
  static const String detail = ShowerDetailScreen.routeName;
  static const String user = UserScreen.routeName;
}