
import 'package:flutter/material.dart';

Widget getDefaultAppBar(String title) {
  return AppBar(
    brightness: Brightness.dark,
    title: Text(title),
  );
}