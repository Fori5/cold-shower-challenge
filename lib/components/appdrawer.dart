import 'dart:ffi';

import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:coldshowerchallenge/screens/about/aboutscreen.dart';
import 'package:flutter/material.dart';

enum DrawerItem { HOME, PROFILE, STATS, LEADERBOARD, SETTINGS, ABOUT }

class AppDrawer extends StatelessWidget {
  AppDrawer({this.currentItem});

  final _drawerKey = GlobalKey<DrawerControllerState>();
  final DrawerItem currentItem;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      key: _drawerKey,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          _createHeader(),
          _createDrawerItem(
              icon: Icons.home,
              text: 'Home',
              item: DrawerItem.HOME,
              onTap: () =>
                  {selectedItem(context, DrawerItem.HOME, Routes.home)}),
          _createDrawerItem(
              icon: Icons.person,
              text: 'Profile',
              item: DrawerItem.PROFILE,
              onTap: () =>
                  {selectedItem(context, DrawerItem.PROFILE, Routes.profile)}),
          _createDrawerItem(
              icon: Icons.assessment,
              text: 'History',
              item: DrawerItem.STATS,
              onTap: () =>
                  {selectedItem(context, DrawerItem.STATS, Routes.stats)}),
          _createDrawerItem(
              icon: Icons.star,
              text: 'Leaderboard',
              item: DrawerItem.LEADERBOARD,
              onTap: () => {
                    selectedItem(context, DrawerItem.LEADERBOARD, Routes.leaderboard)
                  }),
          _createDrawerItem(
              icon: Icons.settings,
              text: 'Settings',
              item: DrawerItem.SETTINGS,
              onTap: () =>
                  {selectedItem(context, DrawerItem.SETTINGS, Routes.settings)}),
          _createDrawerItem(
              icon: Icons.info,
              text: 'About',
              item: DrawerItem.ABOUT,
              onTap: () =>
                  {selectedItem(context, DrawerItem.ABOUT, Routes.about)})
        ],
      ),
    );
  }

  Widget _createHeader() {
    return DrawerHeader(
        child: Column(
      children: [
        Image.asset('assets/small_logo.png', height: 90.0),
        Container(
          margin: const EdgeInsets.only(top: 8.0),
          child: Text(
            'Cold Shower Challenge',
            style: TextStyle(fontSize: 20, color: Color.fromARGB(255, 2, 169, 244)),
          ),
        )
      ],
    ));
  }

  Widget _createDrawerItem(
      {IconData icon, String text, GestureTapCallback onTap, DrawerItem item}) {
    return ListTile(
      leading: Icon(icon),
      title: Text(text),
      selected: currentItem == item,
      onTap: onTap,
    );
  }

  void selectedItem(BuildContext context, DrawerItem item, String route) {
    if (currentItem == item) {
      Navigator.pop(context);
    } else {
      Navigator.pushReplacementNamed(context, route);
    }
  }
}
