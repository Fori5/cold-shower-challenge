import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/models/user.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;

class HomePage extends StatefulWidget {
  static const String routeName = '/home';

  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DateTime currentBackPressTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: AppDrawer(currentItem: DrawerItem.HOME),
        appBar: getDefaultAppBar('Cold Shower Challenge'),
        body: WillPopScope(child: _showContent(), onWillPop: onWillPop));
  }

  Widget _showContent() {
    return Stack(children: [
      Column(
        children: [
          showLogo(),
          Padding(
              padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 20.0),
              child: FutureBuilder(
                future: globals.repository.getUser(globals.userId),
                builder: (context, AsyncSnapshot<User> user) {
                  if (!user.hasData) {
                    return LinearProgressIndicator();
                  }
                  return Column(
                    children: [
                      Text(
                        'User: ' + user.data.name,
                        style: TextStyle(fontSize: 20.0),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Text('Total showers: ' + user.data.totalShowers
                          .toString(),
                          style: TextStyle(fontSize: 20.0)),
                      Container(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Text('Current streak: ' + user.data.streak.toString(),
                          style: TextStyle(fontSize: 20.0)),
                    ],
                  );
                },
              ))
        ],
      ),
      _showStartButton()
    ]);
  }

  Widget showLogo() {
    return new Center(
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Image.asset('assets/shower.png'),
        ),
      ),
    );
  }

  Widget _showStartButton() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
            margin: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
            width: double.infinity,
            height: 50.0,
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              onPressed: () {
                Navigator.pushNamed(context, Routes.shower);
              },
              child: Text(
                'TAKE A SHOWER NOW',
                style: new TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            )));
  }

  Future<bool> onWillPop() {
    if (_scaffoldKey.currentState.isDrawerOpen) {
      return Future.value(true);
    }

    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Tap the back button again to quit.");
      return Future.value(false);
    }
    return Future.value(true);
  }
}
