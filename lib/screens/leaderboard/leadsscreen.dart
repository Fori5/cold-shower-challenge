import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/models/user.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:coldshowerchallenge/screens/profile/userscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;

class LeadsScreen extends StatelessWidget {
  static const String routeName = '/leaderboard';

  @override
  Widget build(BuildContext context) {
    return Leads();
  }
}

class Leads extends StatefulWidget {
  Leads({Key key}) : super(key: key);

  @override
  _LeadsScreenState createState() => _LeadsScreenState();
}

class _LeadsScreenState extends State<Leads> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          key: _scaffoldKey,
          drawer: AppDrawer(currentItem: DrawerItem.LEADERBOARD),
          appBar: AppBar(
            brightness: Brightness.dark,
            bottom: TabBar(
              isScrollable: true,
              indicatorColor: Colors.white,
              labelColor: Colors.white,
              labelPadding: EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
              tabs: [
                Tab(text: 'current streak'),
                Tab(text: 'max streak'),
                Tab(text: 'total time'),
                Tab(text: 'total showers')
              ],
            ),
            title: Text('Leaderboard'),
          ),
          body: WillPopScope(
              child: TabBarView(
                children: [
                  createList('streak',1),
                  createList('max_streak',2),
                  createList('total_time',3),
                  createList('total_showers',4),
                ],
              ),
              onWillPop: () {
                if (!_scaffoldKey.currentState.isDrawerOpen) {
                  Navigator.pushReplacementNamed(context, Routes.home);
                  return Future.value(false);
                }
                return Future.value(true);
              }),
        ));
  }

  Widget createList(String order, int type) {
    return StreamBuilder<QuerySnapshot>(
      stream: globals.repository.getLeaders(order),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();
        final int count = snapshot.data.documents.length;
        return ListView.separated(
            itemBuilder: (context, index) {
              final DocumentSnapshot doc = snapshot.data.documents[index];
              User user = User.fromSnapshot(doc);
              String value = "";
              switch (type) {
                case 1:
                  value = user.streak.toString();
                  break;
                case 2:
                  value = user.maxStreak.toString();
                  break;
                case 3:
                  value = user.totalTime.toString();
                  break;
                case 4:
                  value = user.totalShowers.toString();
                  break;
              }
              return InkWell(
                child: Container(
                    child: Row(children: [
                      Text((index+1).toString() + ". " + user.name, style:
                      TextStyle
                        (fontSize: 16.0,
                          fontWeight: user.id == globals.userId ?
                      FontWeight.bold : FontWeight.normal),),
                      Expanded(
                          flex: 1,
                          child: Text(value, textAlign:
                          TextAlign.right, style: TextStyle(fontSize: 20.0)))
                    ]),
                    padding: EdgeInsets.all(18.0)),
                onTap: () {
                  Navigator.pushNamed(context, Routes.user,
                      arguments: UserDetailArguments(user));
                },
              );
            },
            separatorBuilder: (context, index) => Divider(
                  height: 1.0,
                ),
            itemCount: count);
      },
    );
  }
}
