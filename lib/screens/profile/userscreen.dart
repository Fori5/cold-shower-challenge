import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/models/user.dart';
import 'package:coldshowerchallenge/screens/profile/profilewidget.dart';
import 'package:flutter/material.dart';

class UserScreen extends StatelessWidget {
  static const String routeName = '/user';

  @override
  Widget build(BuildContext context) {
    final UserDetailArguments args =
        ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: getDefaultAppBar(args.user.name),
      body: Profile(args.user.id),
    );
  }
}

class UserDetailArguments {
  final User user;

  UserDetailArguments(this.user);
}


