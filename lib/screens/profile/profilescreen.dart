import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:coldshowerchallenge/screens/profile/profilewidget.dart';
import 'package:flutter/material.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;

class ProfileScreen extends StatelessWidget {
  static const String routeName = '/profile';
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(currentItem: DrawerItem.PROFILE),
      appBar: getDefaultAppBar('Profile'),
      body: WillPopScope(
          child: Profile(globals.userId),
          onWillPop: () {
            if (!_scaffoldKey.currentState.isDrawerOpen) {
              Navigator.pushReplacementNamed(context, Routes.home);
              return Future.value(false);
            }
            return Future.value(true);
          }),
    );
  }
}
