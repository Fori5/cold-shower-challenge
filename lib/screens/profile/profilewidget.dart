import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/models/user.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;

class Profile extends StatefulWidget {
  final String _userId;
  Profile(this._userId, {Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState(_userId);
}

class _ProfileScreenState extends State<Profile> {
  final String _userId;

  _ProfileScreenState(this._userId);

  @override
  void initState() {
    super.initState();
    getUser();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 20.0),
        child: FutureBuilder(
          future: globals.repository.getUser(_userId),
          builder: (context, AsyncSnapshot<User> user) {
            if (!user.hasData) {
              return LinearProgressIndicator();
            }
            return Column(
              children: [
                Text(
                  'User: ' + user.data.name,
                  style: TextStyle(fontSize: 20.0),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Text('Total showers: ' + user.data.totalShowers
                    .toString(),
                    style: TextStyle(fontSize: 20.0)),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Text('Current streak: ' + user.data.streak.toString(),
                    style: TextStyle(fontSize: 20.0)),
              ],
            );
          },
        ));
  }

  void getUser() async {
    FirebaseUser user = await globals.auth.getCurrentUser();
    print("displayName=${user.displayName}");
  }
}
