import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/models/shower.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ShowerDetailScreen extends StatelessWidget {
  static const String routeName = '/showerDetail';

  @override
  Widget build(BuildContext context) {
    final ShowerDetailArguments args =
        ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: getDefaultAppBar('Shower Detail'),
        body: ShowerDetail(args.shower));
  }
}

class ShowerDetail extends StatefulWidget {
  final Shower shower;

  ShowerDetail(this.shower, {Key key}) : super(key: key);

  @override
  _ShowerDetailScreenState createState() => _ShowerDetailScreenState(shower);
}

class _ShowerDetailScreenState extends State<ShowerDetail> {
  final Shower _shower;

  _ShowerDetailScreenState(this._shower);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          child:  Text(
            'Start: ' + DateFormat().format(_shower.start),
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 17.0),
          ),
          padding: EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 12.0),
        ),
        Divider(),
        Container(
          child:  Text(
            'End: ' + DateFormat().format(_shower.end),
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 17.0),
          ),
          padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
        ),
        Divider(),
        Container(
          child:  Text(
            'Duration: ' + _shower.printDuration(),
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 17.0),
          ),
          padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
        ),
        Divider(),
        Container(
          child:  Text(
            'Note:',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 17.0),
          ),
          padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
        ),
        Container(
          child: Text(_shower.note,
            textAlign: TextAlign.justify,),
          padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 20.0),
        )
      ],
    );
  }
}

class ShowerDetailArguments {
  final Shower shower;

  ShowerDetailArguments(this.shower);
}
