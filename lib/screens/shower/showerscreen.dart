import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:flutter/material.dart';

class ShowerScreen extends StatelessWidget {
  static const String routeName = '/shower';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getDefaultAppBar('Shower'),
        body: Shower());
  }
}

class Shower extends StatefulWidget {
  Shower({Key key}) : super(key: key);

  @override
  _ShowerScreenState createState() => _ShowerScreenState();
}

class _ShowerScreenState extends State<Shower> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('shower'),
      ],
    );
  }
}
