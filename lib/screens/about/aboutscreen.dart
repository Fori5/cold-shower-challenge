import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  static const String routeName = '/about';
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(currentItem: DrawerItem.ABOUT),
      appBar: getDefaultAppBar('About'),
      body: WillPopScope(
          child: About(),
          onWillPop: () {
            if (!_scaffoldKey.currentState.isDrawerOpen) {
              Navigator.pushReplacementNamed(context, Routes.home);
              return Future.value(false);
            }
            return Future.value(true);
          }),
    );
  }
}

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<About> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('about content'),
        Text('Icons made by Smashicons from www.flaticon.com')
      ],
    );
  }
}
