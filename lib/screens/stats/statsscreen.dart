import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/models/shower.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:coldshowerchallenge/screens/shower/detailscreen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;

class StatsScreen extends StatelessWidget {
  static const String routeName = '/stats';
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(currentItem: DrawerItem.STATS),
      appBar: getDefaultAppBar('History'),
      body: WillPopScope(
          child: Stats(),
          onWillPop: () {
            if (!_scaffoldKey.currentState.isDrawerOpen) {
              Navigator.pushReplacementNamed(context, Routes.home);
              return Future.value(false);
            }
            return Future.value(true);
          }),
    );
  }
}

class Stats extends StatefulWidget {
  Stats({Key key}) : super(key: key);

  @override
  _StatsScreenState createState() => _StatsScreenState();
}

class _StatsScreenState extends State<Stats> with TickerProviderStateMixin {
  Map<DateTime, List> _events;
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;
  DateTime _selectedDay;

  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();

    _events = {};
    _selectedEvents = _events[_selectedDay] ?? [];
    _calendarController = CalendarController();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    _animationController.forward();
    _onVisibleDaysChanged(
        DateTime(_selectedDay.year, _selectedDay.month),
        DateTime(_selectedDay.year, _selectedDay.month + 1),
        CalendarFormat.month);
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      _selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    globals.repository
        .getMappedShowers(globals.userId, first, last)
        .then((value) {
      setState(() {
        _events = value;
        _selectedEvents = value[DateTime(
                _selectedDay.year, _selectedDay.month, _selectedDay.day)] ??
            [];
      });
    });
  }

  void _onCalendarCreated(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onCalendarCreated');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        _buildTableCalendar(),
        Divider(color: Colors.grey, height: 1.0),
        Expanded(child: _buildEventList()),
      ],
    );
  }

  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(
      locale: 'en_US',
      calendarController: _calendarController,
      events: _events,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.horizontalSwipe,
      availableCalendarFormats: const {
        CalendarFormat.month: 'Month',
        CalendarFormat.week: 'Week',
      },
      calendarStyle: CalendarStyle(
        selectedColor: Colors.blue,
        todayColor: Colors.lightBlue[200],
        markersColor: Colors.orangeAccent,
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  Widget _buildEventList() {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(
        height: 1.0,
      ),
      itemCount: _selectedEvents.length,
      itemBuilder: (context, index) {
        Shower event = _selectedEvents[index];
        return InkWell(
          child: Container(
              child: Row(children: [
                Text('Shower #' + (index + 1).toString()),
                Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            'start: ' + DateFormat('HH:mm:ss').format(event
                        .start),
                            textAlign: TextAlign.right,
                          ),
                          alignment: Alignment.topRight,
                        ),
                        Container(
                          height: 20.0,
                          child: Text(
                            'duration: '+  event.printDuration(),
                            textAlign: TextAlign.right,
                          ),
                          alignment: Alignment.bottomRight,
                        ),
                      ],
                    ))
              ]),
              padding: EdgeInsets.all(8.0)),
          onTap: () {
            Navigator.pushNamed(context, Routes.detail,
                arguments: ShowerDetailArguments(event));
          },
        );
        return ListTile(
          title: Text(event.toString()),
          onTap: () {
            Navigator.pushNamed(context, Routes.detail,
                arguments: ShowerDetailArguments(event));
          },
        );
      },
    );
  }
}
