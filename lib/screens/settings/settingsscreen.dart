import 'package:coldshowerchallenge/components/appbar.dart';
import 'package:coldshowerchallenge/components/appdrawer.dart';
import 'package:coldshowerchallenge/components/faderoute.dart';
import 'package:coldshowerchallenge/routes/routes.dart';
import 'package:coldshowerchallenge/screens/root/rootscreen.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:coldshowerchallenge/globals.dart' as globals;

class SettingsScreen extends StatelessWidget {
  static const String routeName = '/settings';
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(currentItem: DrawerItem.SETTINGS),
      appBar: getDefaultAppBar('Settings'),
      body: WillPopScope(
          child: Settings(),
          onWillPop: () {
            if (!_scaffoldKey.currentState.isDrawerOpen) {
              Navigator.pushReplacementNamed(context, Routes.home);
              return Future.value(false);
            }
            return Future.value(true);
          }),
    );
  }

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class Settings extends StatefulWidget {
  Settings({Key key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<Settings> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SettingsList(
      sections: [
        SettingsSection(
          title: 'Account',
          tiles: [
            SettingsTile(title: 'Email'),
            SettingsTile(title: 'Name', subtitle: 'Tap to change'),
            SettingsTile(title: 'Password', subtitle: 'Tap to change'),
            SettingsTile(
              title: 'Sign out',
              leading: Icon(Icons.exit_to_app),
              onTap: () {
                globals.auth.signOut();
                Navigator.pushReplacement(
                    context, FadeRoute(page: RootScreen(auth: globals.auth)));
              },
            )
          ],
        ),
        SettingsSection(
          title: 'Notification',
          tiles: [
            SettingsTile.switchTile(title: 'Enabled', onToggle: (on){
              print(on);
            },
                switchValue: false, enabled: true),
            SettingsTile(title: 'Time')
          ],
        )
      ],
    );
  }
}
