import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coldshowerchallenge/components/customwebview.dart';
import 'package:coldshowerchallenge/models/user.dart';
import 'package:coldshowerchallenge/repository/datarepository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> loginWithFacebook(BuildContext context);

  Future<String> signUp(String email, String password, String name);

  Future<FirebaseUser> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final String _clientId = "2685020315107424";
  final String _redirectUrl =
      "https://www.facebook.com/connect/login_success.html";
  final DataRepository repository;

  Auth(this.repository);

  Future<String> signIn(String email, String password) async {
    AuthResult result = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    return user.uid;
  }

  Future<String> signUp(String email, String password, String name) async {
    AuthResult result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    if (user.uid != null) {
      UserUpdateInfo info = UserUpdateInfo();
      info.displayName = name;
      user.updateProfile(info);
      await _createUserInFireStore(user.uid, name);
      return signIn(email, password);
    }
    return user.uid;
  }

  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.isEmailVerified;
  }

  Future<String> loginWithFacebook(BuildContext context) async {
    String result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                CustomWebView(
                  selectedUrl:
                  'https://www.facebook.com/dialog/oauth?client_id=$_clientId&redirect_uri=$_redirectUrl&response_type=token&scope=email,public_profile,',
                ),
            maintainState: true));
    if (result != null) {
      try {
        final facebookAuthCred =
        FacebookAuthProvider.getCredential(accessToken: result);
        AuthResult res =
        await _firebaseAuth.signInWithCredential(facebookAuthCred);
        FirebaseUser user = res.user;
        await _createUserInFireStore(user.uid, user.displayName);
        return user.uid;
      } catch (e) {
        return Future.value(null);
      }
    }
  }

  void _createUserInFireStore(String uid, String name) async {
    DocumentSnapshot value = await repository.collection.document(uid).get();
    if (!value.exists) {
      repository.collection.document(uid).setData({
        'name': name,
        'id': uid,
        'last': null,
        'streak': 0,
        'max_streak': 0,
        'streak_start': null,
        'total_showers': 0,
        'total_time': 0
      });
    }
    //repository.collection.document(uid).setData({
    //  'name': name,
    //  'age': 23
    //});
    //repository.collection.add({'name': 'hovno', 'age': 69});

    //repository.addUser(User(user.displayName)).then((value) => print('added '
    //    + value.toString()));
  }
}
