import 'package:flutter/material.dart';

ThemeData appTheme(BuildContext context) {
  return ThemeData(
      //primaryColor: Colors.blue,
      primaryColor: Color.fromARGB(255, 2, 169, 244),
      primaryColorDark: Color.fromARGB(255, 6, 136, 194),
      visualDensity: VisualDensity.adaptivePlatformDensity,
      primaryIconTheme:
          Theme.of(context).primaryIconTheme.copyWith(color: Colors.white),
      primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)));
}
