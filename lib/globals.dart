library globals;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coldshowerchallenge/repository/datarepository.dart';
import 'package:coldshowerchallenge/services/authentication.dart';


final DataRepository repository = DataRepository(_initFirestore());
BaseAuth auth;
String userId;

Firestore _initFirestore() {
  Firestore firestore = Firestore.instance;
  firestore.settings(persistenceEnabled: true, cacheSizeBytes: 10000000);
  return firestore;
}