import 'package:cloud_firestore/cloud_firestore.dart';

class Shower {
  DateTime start;
  DateTime end;
  int time;
  String note;

  DocumentReference reference;

  Shower(this.start, this.end, this.time, {this.note = '', this.reference});

  factory Shower.fromJson(Map<dynamic, dynamic> json) => _showerFromJson(json);

  factory Shower.fromSnapshot(DocumentSnapshot snapshot) {
    Shower shower = Shower.fromJson(snapshot.data);
    shower.reference = snapshot.reference;
    return shower;
  }

  Map<String, dynamic> toJson() => _showerToJson(this);

  String printDuration() {
    Duration duration = Duration(seconds: this.time);
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  @override
  String toString() => "Shower, start=$start end=$end time=$time";
}

Shower _showerFromJson(Map<dynamic, dynamic> json) {
  return Shower((json['start'] as Timestamp).toDate(),
      (json['end'] as Timestamp).toDate(), json['time'] as int,
      note: json['note'] as String);
}

Map<String, dynamic> _showerToJson(Shower shower) => <String, dynamic>{
      'start': shower.start,
      'end': shower.end,
      'time': shower.time,
      'note': shower.note
    };