import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coldshowerchallenge/models/shower.dart';

class User {
  String name;
  String id;
  int streak;
  int maxStreak;
  int totalShowers;
  int totalTime;
  DateTime streakStart;
  DateTime lastShower;

  List<Shower> showers = List<Shower>();

  DocumentReference reference;

  User(this.name,
      {this.id,
      this.streak,
      this.maxStreak,
      this.totalShowers,
      this.totalTime,
      this.streakStart,
      this.lastShower,
      this.showers,
      this.reference});

  factory User.fromJson(Map<String, dynamic> json) => _userFromJson(json);

  factory User.fromSnapshot(DocumentSnapshot snapshot) {
    User user = User.fromJson(snapshot.data);
    user.reference = snapshot.reference;
    return user;
  }

  Map<String, dynamic> toJson() => _userToJson(this);

  @override
  String toString() => 'User: $name';
}

User _userFromJson(Map<String, dynamic> json) {
  return User(json['name'] as String,
      id: json['id'] as String,
      streak: json['streak'] as int,
      maxStreak: json['max_streak'] as int,
      totalShowers: json['total_showers'] as int,
      totalTime: json['total_time'] as int,
      streakStart: json['streak_start'] == null
          ? null
          : (json['streak_start'] as Timestamp).toDate(),
      lastShower:
          json['last'] == null ? null : (json['last'] as Timestamp).toDate(),
      showers: _convertShowers(json['showers'] as List));
}

List<Shower> _convertShowers(List showerMap) {
  if (showerMap == null) {
    return null;
  }
  List<Shower> showers = List<Shower>();
  showerMap.forEach((value) {
    showers.add(Shower.fromJson(value));
  });
  return showers;
}

Map<String, dynamic> _userToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'streak': instance.streak,
      'max_streak': instance.maxStreak,
      'total_showers': instance.totalShowers,
      'total_time': instance.totalTime,
      'streak_start': instance.streakStart,
      'last': instance.lastShower,
      'showers': _showersList(instance.showers)
    };

List<Map<String, dynamic>> _showersList(List<Shower> showers) {
  if (showers == null) {
    return null;
  }
  List<Map<String, dynamic>> showerMap = List<Map<String, dynamic>>();
  showers.forEach((shower) {
    showerMap.add(shower.toJson());
  });
  return showerMap;
}
