import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coldshowerchallenge/models/shower.dart';
import 'package:coldshowerchallenge/models/user.dart';

class DataRepository {
  final CollectionReference collection;

  DataRepository(Firestore firestore)
      : collection = firestore.collection('users');

  // Future<DocumentSnapshot> getUser(userId) {
  //   return collection.document(userId).get(source: Source.serverAndCache);
  // }

  Future<User> getUser(userId) async {
    DocumentSnapshot doc =
        await collection.document(userId).get(source: Source.serverAndCache);
    return Future.value(User.fromSnapshot(doc));
  }

  Future<QuerySnapshot> getShowers(userId) async {
    return collection.document(userId).collection('showers').getDocuments();
  }

  Future<Map<DateTime, List<dynamic>>> getMappedShowers(userId, DateTime 
  first, DateTime last) async {
    QuerySnapshot snapshot =
        await collection.document(userId).collection('showers').where
          ('start', isGreaterThanOrEqualTo: DateTime(first.year, first.month,
            first.day).subtract(Duration(days: 1))).where('start',
            isLessThanOrEqualTo: DateTime(last.year, last.month, last.day).add(Duration(days: 1)))
            .getDocuments();
    var showers = snapshot.documents.map((e) => Shower.fromSnapshot(e));
    Map<DateTime, List<dynamic>> e = {};
    for (var shower in showers) {
      e
          .putIfAbsent(
              DateTime(shower.start.year, shower.start.month, shower.start.day),
              () => [])
          .add(shower);
    }
    return Future.value(e);
  }

  Stream<QuerySnapshot> getLeaders(String order) {
    return collection.orderBy(order, descending: true).snapshots();
  }

  Stream<QuerySnapshot> getStream() {
    return collection.snapshots();
  }

  Future<DocumentReference> addUser(User user) {
    return collection.add(user.toJson());
  }

  updateUser(User user) async {
    await collection
        .document(user.reference.documentID)
        .updateData(user.toJson());
  }
}
